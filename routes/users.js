var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send({
    name: "Khanh Đẹp trai",
    email: "khanh.dep.trai@kmin.edu.vn",
    avatar: "https://media.tenor.com/7-yDjCz6z3gAAAAM/test.gif"
  });
});

module.exports = router;
